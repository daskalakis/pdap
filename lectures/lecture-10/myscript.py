import numpy as np
import matplotlib.pyplot as plt

firstvariable = 'a'
secondvariable = 123.45

def something_else(a):
    return 2 * a

print(something_else(firstvariable))
print("and")
print(something_else(secondvariable))

plt.plot([1, 4, 6])
plt.savefig('myfigure.png')