***********************************
Practical Data Analysis with Python
***********************************

:Author: Dr. Andreas Hilboll
:Email:  hilboll@uni-bremen.de
:Date:   Winter term 2019/20

This Git repository holds the material for the course `Practical Data Analysis with Python <https://elearning.uni-bremen.de/dispatch.php/course/overview?cid=6a8b95d3c5fa0082b53ae307e49f9701>`__ by `Dr. Andreas Hilboll <http://www.iup.uni-bremen.de/~hilboll>`__, given at the `University of Bremen <http://www.uni-bremen.de/>`__ during winter term 2019.


General Information
=====================

Time
  Tuesday, 08:15 - 09:45

Dates
  15 Oct 2019 - 28 Jan 2020

Location
  `NW1 / S1360 <https://oracle-web.zfn.uni-bremen.de/web/lageplan?pi_raum_id=8474&pi_raumnummer=S1360&pi_anz=1>`__

This course aims to be a very practical introduction to using a
computer in scientific data analysis, using the `Python
<https://www.python.org/>`__ programming language. The course will be
most beneficial to second semester students, as they will be able to
directly apply the newly learned techniques in their subsequent
M.Sc. thesis work.

The course will be accessible to all students, as it will not make any
assumptions on the students' computer environment, i.e., all topics will be
explained for all relevant operating systems (Windows, MacOS, Linux).

**Participants are expected to bring their own laptops, as the course
contains large parts of practical work.**


Links
=====

:Course repository: http://unihb.eu/pdap2019
:JupyterLab server: http://unihb.eu/lamoslab


Syllabus
========

The first part of the course will touch on the following subjects:

- *Jupyter Notebook*: interactive, web-based usage of Python
- Getting started: How to setup your own computer for data analysis in Python.
- Hands-on introduction to the Python scientific ecosystem: Arrays and
  mathematical operations.
- Labeled arrays, or how to intuitively work with data.
- Reading and writing data in common file formats.
- Notebooks, script, and modules: How to properly organize your code
- *But this worked yesterday, before I made some changes ...*, or: an
  introduction to version control.
- Making both beautiful and meaningful plots from data.
- An overview of the most common special-topic libraries for all research areas
  covered by the pep_ program.

.. _pep: http://www.pep.uni-bremen.de/

In its last sessions, the course will focus on a practical introduction to the
most common data analysis tasks, like, among others, curve fitting, parameter
estimation, and correlation analysis.

Every week, there will be 2 hours of course (approx. 1 hour lecture + 1 hour
practical exercises). There will be weekly homework excercises, plus two graded
homework projects.


(almost) weekly homework
========================

50% of the (almost) weekly homework exercises have to be completed in order to complete the "Studienleistung" (which you need in order to get awared CPs for this course).
